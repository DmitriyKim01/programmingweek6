package week6group;

// Dmitriy Kim

import org.junit.Test;
import static org.junit.Assert.*;

public class SphereTest {
    // Testing the constructor
    @Test
    public void constructorTest(){
        Sphere sphere = new Sphere(5);
        assertEquals( "Testing the radius", 5, sphere.getRadius(), 0);
    }

    @Test 
    public void surfaceAreaTest(){
        Sphere sphere = new Sphere(5);
        assertEquals( "Testing area", 100 * Math.PI, sphere.getSurfaceArea(), 0);
    }
    @Test
    public void volumeTest(){
        Sphere sphere = new Sphere(5);
        assertEquals( "Testing volume", Math.PI*Math.pow(5, 3), sphere.getVolume(), 0);
    }
}
