//Knadry Soufiane
package week6group;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CylinderTest {
    @Test
    public void constructorTest(){
        
        Cylinder cylinder = new Cylinder(1, 1);
        assertEquals( 1, cylinder.getRadius(), 0);
        assertEquals( 1, cylinder.getHeight(), 0);
    }
    @Test 
    public void surfaceAreaTest(){
        Cylinder cylinder = new Cylinder(1,1);
        assertEquals( 4*Math.PI, cylinder.getSurfaceArea(), 0);
    }
    @Test
    public void volumeTest(){
        Cylinder cylinder = new Cylinder(1,1);
        assertEquals( Math.PI, cylinder.getVolume(), 0);
    }
}
