package week6group;

/**
 * Represents a cylinder in three-dimensional space.
 * This class implements the Shape3D interface and provides methods to calculate
 * the volume and surface area of the cylinder.
 * 
 * @author Dmitriy Kim
 */

public class Cylinder implements Shape3D{
    private double radius;
    private double height;
    final private double PI;

    public Cylinder(double radius, double height){
        this.radius = radius;
        this.height = height;
        this.PI = Math.PI;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getHeight(){
        return this.height;
    }
    /**
     * Calculates the volume of the cylinder.
     * 
     * @return The volume of the cylinder.
     * @throws UnsupportedOperationException if the method is not implemented yet.
     */

    @Override
    public double getVolume(){
        return this.PI * Math.pow(this.radius, 2) * this.height;
    }

    /**
     * Calculates the surface area of the cylinder.
     *  
     * @return The surface area of the cylinder.
     * @throws UnsupportedOperationException if the method is not implemented yet.
     */

    @Override
    public double getSurfaceArea(){
        return 2 * this.PI * this.radius * (this.radius + this.height);
    }
}
