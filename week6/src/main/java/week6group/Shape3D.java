package week6group;

public interface Shape3D {
    double getVolume();
    double getSurfaceArea();
}
