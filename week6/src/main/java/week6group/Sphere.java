//Knadry Soufiane
package week6group;
/**
 * Represents a Sphere in three-dimensional space.
 * This class implements the Shape3D to provide the methods for the necessary calculations
 * 
 * @author Knadry Soufiane
 */
public class Sphere implements Shape3D {

    private double radius;


    public Sphere(double radius){
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    
    
    @Override
    public double getVolume() {
        return (Math.PI*Math.pow(radius, 3));
    }

   
    @Override
    public double getSurfaceArea() {
        return (4*Math.PI*Math.pow(radius, 2));
    }

    
  
}
